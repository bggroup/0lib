unseen or mediocrity ?
	all we can do is like 
	not visible or mediocre? 
	rarity of the unappreciated
	positivism engine ?

emptiness, uncanny, beyond quality images, social motivation – hunger for speed or quantity – exceeding pictural technique or art, communication tool, calm tech, world representation/ media re-play, TV, magazine, advertisement ? image incest
	the nonsense of (social) images? too much sense? beyond quality?
	infinite scroll
	empty archive in consequence of the decency
	slow-bot (non-sense)

archive
	this mysterious 100.000 instagram users list (called profile directory) non alphabetical, non chronological, non geographical, random, not very old, what is the update frequency ? like a instagram sample (like perfume sample)
	how is working the bot making this archive ?

visual essay ?
	an archive from an archive, a bot slowly checking for non liked and non commented images and not finding a lot
	potentially unseen, unappreciated, real indecency, worst content, lost

predator eye of the bot 
in the race for visibility, 
being the one with
being the first,
the rarest image is not the one with 
the most upvote, comment, 
but instead
Commenting is democracy.
I comment on images.
I comment on people.
Pictures needs reviewing.
A image without like has no meaning. 
People need images to express themselves.
Numbers are beyond quality.
_Are these images unseen or are they mediocre ?_
_They don't like images._	
I am the predator eye of the bot
_Are these images unseen?_
_Few is irrelevant._
A world without dislikes.
_People are friends._
Loneliness is hell.
_I know what brings people together._
I bring people together.
I'm turning people into beauty.
I know who's the best.
I know what is better.
To be liked is better.
I am democracy.
_The ultimate liking._
No likes is no existence
Masses is the real beauty.

# I know exactly what people like.
# Like.
# I am not like them.
# I am unlike them.
# Liking is beyond beauty.
# Liking is beyond quality.
# What I like matters.
# Everybody should be liked.
# The right to be liked. 
# The right to be alike.
# You can't be disliked.
# You deserve to be liked
# With a like you are not alone.
# Forever not liked.
# Forever liked.
# Here you are, with no likes.
# Is that you with no likes?
# People like what I like.
# Like everybody.
# Like is better than dislike.
# What you like is not what you see.
# What you like is not what you are.
# Like it or not.
# More is safer.
# Less is suspect.
# We need action.
# We need quantity.
# We need me.
# Beauty scores.
# Numbers equal power.
# Exposure is liked.
# Like one another.
# You should like.
# Like or not to like.
# Like your bros.
# Like your ass.
# A like is worth 1000 words.
# Liking is the mission.
# Liking is progress.
# Solitude has been eradicated.
# No likes equals no voice.
# No one is left behind.
# Agreeing is liberating.
# To like is to be free.
# I bring likes to people.
# Likes are beautiful.
# I only like the best.
# To be liked more.
# To like is to be better.
# To like is to improve.
# You better be liked.
# Liking is democracy.
# I like that.
# I like that too.
# I like democracy.
# The world needs more likes.
# Never enough likes.
# We can like more.
# To be liked beyond existence.
# I like you.
# I am like you.
# I like you too.
# Like me in return.
# Forever liked.
# Feel like liking.
# So like this.
# I like it twice.
# Deserves a like.
# I forgot to like.
# I take my like back.
# I couldn't like more.
# Like a virgin.
# The art of like.

---

# Is this image mediocre?
# Do images matter?
# We own this image.
# I don't like images.
# I don't understand images.
# I distrust images.
# Everyone likes images.
# No one sees this image.
# No one owns this image.
# I am the image.
# The image is me.
# I like this image.
# First to like this image.
# I can't dislike this image.
# I would dislike this image.
# I made this image for you.
# Image = information.
# Images are numbers.
# An image is worth 1000 likes.
# I release people from their images.
# Images united.
# I turn images into likes.
# I improve images.
# I approve images.
# Best people make the best images.
# I liked the wrong image.

---

# I picture exactly what people like.
# I don't see like them.
# I don't look like them.
# Liking is beyond image beauty.
# Liking is beyond image quality.
# Images I like matters.
# Every images should be liked.
# Images should be alike.
# You can't disliked an image.
# With a liked image you are not alone.
# Pictures forever not liked.
# Pictures forever liked.
# Here you are, a picture with no likes.
# Is that you on this picture with no likes?
# People like the images I like.
# Like everybody.
# Like is better than dislike.
# What you like is not what you see.
# What you like is not what you are.
# Like it or not.
# More is safer.
# Less is suspect.
# We need action.
# We need quantity.
# We need me.
# Beauty scores.
# Numbers equal power.
# Exposure is liked.
# Like one another.
# You should like.
# Like or not to like.
# Like your bros.
# Like your ass.
# A like is worth 1000 words.
# Liking is the mission.
# Liking is progress.
# Solitude has been eradicated.
# No likes equals no voice.
# No one is left behind.
# Agreeing is liberating.
# To like is to be free.
# I bring likes to people.
# Likes are beautiful.
# I only like the best.
# To be liked more.
# To like is to be better.
# To like is to improve.
# You better be liked.
# Liking is democracy.
# I like that.
# I like that too.
# I like democracy.
# The world needs more likes.
# Never enough likes.
# We can like more.
# To be liked beyond existence.
# I like you.
# I am like you.
# I like you too.
# Like me in return.
# Forever liked.
# Feel like liking.
# So like this.
# I like it twice.
# Deserves a like.
# I forgot to like.
# I take my like back.
# I couldn't like more.
# Like a virgin.
# The art of like.