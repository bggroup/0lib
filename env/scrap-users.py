#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import json
import datetime
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from pprint import pprint

date = '{:%Y-%m-%d}'.format(datetime.datetime.now())
time = '{:%H-%M-%S}'.format(datetime.datetime.now())

with open('data.json') as f:
    data = json.load(f)

print(date)
print(time)

def saveFile(path,content,mode):
    with open(path, mode) as f:
        f.write(content)
        f.close()

def browserSoup(url,options):
    browser = webdriver.Firefox(firefox_options=options)
    browser.get(url)
    html = browser.page_source
    soup = BeautifulSoup(html, 'html.parser')
    browser.quit()
    return soup

options = Options()
options.add_argument("--headless")
instagramUrl = 'https://www.instagram.com'
profilesUrl = instagramUrl + '/directory/profiles/'

# iterate

soup = browserSoup(profilesUrl,options)
directories = soup.find_all(class_="_rtyxt")
for i in directories:
        print(instagramUrl + i['href'])
        data[instagramUrl + i['href']] = {}

        soup = browserSoup(instagramUrl + i['href'],options)
        subdirectories = soup.find_all(class_="_rtyxt")
        for j in subdirectories: 
            print(instagramUrl + j['href'])
            data[instagramUrl + i['href']][instagramUrl + j['href']] = {}
        
            soup = browserSoup(instagramUrl + j['href'],options)
            profiles = soup.find_all(class_="_rtyxt")
            for k in profiles: 
                print(k.contents[0])
                data[instagramUrl + i['href']][instagramUrl + j['href']][k.contents[0]] = {}
                data[instagramUrl + i['href']][instagramUrl + j['href']][k.contents[0]]['url'] = instagramUrl + k['href']

        with open('data.json', 'w') as f:
            json.dump(data, f, indent=4)
