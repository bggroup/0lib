#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import json
import datetime
import time
import requests
import urllib.request
import random
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from pprint import pprint

# CONFIG

# set size of driver
size = [1366,768]

# uncomment second line for headless driver
options = Options()
#options.add_argument("--headless")

# VARIABLES

history = [] 

# RUN

driver = webdriver.Firefox(firefox_options=options)
driver.set_window_position(0, 0)
driver.set_window_size(size[0], size[1])
driver.set_page_load_timeout(120)

def login(username,password):
    driver.get('https://www.instagram.com/accounts/login/')
    time.sleep(2)

    uInput = driver.find_element_by_name('username')
    uInput.send_keys(username)
    pInput = driver.find_element_by_name('password')
    pInput.send_keys(password)
    logBtn = driver.find_element_by_xpath("//button[contains(.,'Log in')]")
    logBtn.click()

def user(username):
    print(username)
    
    if history.count(username) > 5:
        user(random.choice(history))

    url = 'https://www.instagram.com/' + username
    driver.get(url)
    time.sleep(2)
   
    if username not in history:
        getPosts(username)

    follower = getFollower()
    
    history.append(username)
    user(follower)

def prev():
       user(history[-1])

def getPosts(username):
    posts = driver.find_elements_by_class_name('_tn0ps');
    
    if not posts:
        print('no posts')
        prev()
    else:
        for i, post in enumerate(posts):
            print(i)
            driver.execute_script("arguments[0].scrollIntoView();", post)
            hover = ActionChains(driver).move_to_element(post);
            hover.perform()
      
            data = getData(post)
            try:
                if data['first'] == '0' and data['second'] == '0':
                    if data['typ'] == 'image':
                        saveImage(username, post, i)
                    if data['typ'] == 'video':
                        saveVideo(username, post, i)
            except:
                print('pass')

def getFollower():
    followersBtn = driver.find_element_by_xpath('/html/body/span/section/main/article/header/section/ul/li[2]/a')
    if followersBtn:
        followersBtn.click()
        time.sleep(2)
        followers = driver.find_elements_by_class_name('_2g7d5')
        
        if not followers:
            print('no followers')
            prev()
        else:
            selected = random.choice(followers)
            follower = selected.text
            return follower


def getData(post):
    data = {}
    icon = post.find_elements_by_class_name('_lxd52')
    if icon:
        classn = icon[0].find_elements_by_tag_name('div')[0].get_attribute('class')
        if classn == '_u3xlu':
            data['typ'] = 'video'
        elif classn == '_jnyq2':
            data['typ'] = 'diapo'
    else:
        data['typ'] = 'image'
    try:
        data['first'] = post.find_elements_by_class_name('_lpowm')[0].find_elements_by_tag_name('li')[0].find_elements_by_tag_name('span')[0].text
        data['second'] = post.find_elements_by_class_name('_lpowm')[0].find_elements_by_tag_name('li')[1].find_elements_by_tag_name('span')[0].text
        data['first'] = data['first'].replace(" ", "")
        data['first'] = data['first'].replace(",", "")
        data['first'] = data['first'].replace(".", "")
        data['first'] = data['first'].replace("k", "000")
        data['second'] = data['second'].replace(" ", "")
        data['second'] = data['second'].replace(",", "")
        data['second'] = data['second'].replace(".", "")
        data['second'] = data['second'].replace("k", "000")
        print(data['typ'], data['first'], data['second'])
        return data
    except:
        print("can't find data")

def saveImage(username, post, i):
    if not os.path.exists('data/' + username):
        os.makedirs('data/' + username)
    img = post.find_element_by_class_name('_2di5p')
    srcset = img.get_attribute('srcset')
    srcsets = srcset.split(',')
    src = srcsets[-1].split(' ')[0]
    print(src)
    if not os.path.exists('data/' + username + '/' + str(i)):
        urllib.request.urlretrieve(src, 'data/' + username + '/' + str(i))

def saveVideo(username, post, i):
    if not os.path.exists('data/' + username):
        os.makedirs('data/' + username)
    post.find_element_by_tag_name('a')[0].click()
    time.sleep(2)
    video = driver.find_elements_by_tag_name('video');
    src = video.get_attribute('src')
    print(src)
    if not os.path.exists('data/' + username + '/' + str(i)):
        urllib.request.urlretrieve(src, 'data/' + username + '/' + str(i))


# INIT

login('notjeanmorgane','London WC2R 1LA')
time.sleep(2)
user('ketlinsanttana')
#username = 'baptiste11ans'
#user(username)
