import time 
import random
import json
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
import os

rdm=['~','#','{','[','|','`','\\','^','@',']','}','^','¤','%','*','µ','£','¨','§','€']
likes=[' ','♥']
loadLevels=[' ','\\','|','/','-']
grayscale=[' ','.',':','-',';','/','░','X','▒','▓','█']
radical=[' ','░','▓','█']
radicalInvert=['█','▓','░',' ']
#sentences = [' Nobody like this picture',' I know exactly what people like.',' Are these pictures unseen ?',' Are these pictures mediocre ?',' Does pictures really matter ?',' I own this picture.',' I dont like pictures.',' I like.',' I am not like them.',' They dont like pictures.',' Everybody likes pictures.',' Are these pictures unseen or are they mediocre?',' What I like is beyond quality.',' Everybody need to be liked.',' Im here when nobody like u.',' People like what I like.',' I like this picture.',' I like everybody.',' I know exactly what people like.',' Like is better than not like.',' What you see isn\'t what you like.',' I made this picture for you.',' Picture is information.',' Quantity is safety.',' Less is obscene.',' Few is ugly.',' People need motion.',' People need quantity.',' People need me.',' Pictures are numbers.',' Strong is beauty.',' Numbers are power.',' Exposure is what people need.',' People have to like.',' People need to like each other.',' I free people from pictures.',' A world without dislikes is a perfect world.',' Liking is the journey.',' Liking lead forward.',' Liking is progress.',' Don\'t forget to like.',' People is friends.',' You can\'t be alone.',' Loneliness is hell.',' Solitude has been eradicated.',' With me people are never alone.',' Forever alone.',' Masses is the real beauty.',' People can\'t be left behind.',' Everyone agreeing is freedom.',' I know what brings people together.',' Pictures bring people together.',' I\'m turning pictures into beauty.',' I\'m turning people into beauty.',' I bring beauty to people.',' Pictures are beautiful.',' I know which pictures are the best.',' I know who\'s the best.',' Best people make the best pictures.',' To like is to be better.',' I know what is better.',' To be liked is to be best.',' Democracy is liking.',' I like people.',' I am democracy.',' World lacks likes.',' There\'s never enough likes.',' Commenting is democracy.',' I comment pictures.',' I comment people.',' Pictures needs review.',' Picture without comments.',' People needs review.',' Pictures don\'t make sense without comments.',' People need pictures to express themselves.',' Numbers are beyond quality.']
sentences = ['I know exactly what people like.','Is this image mediocre?','Do images matter?','We own this image.','I don’t like images.','I don’t understand images.','I distrust images.','Like.','I am not like them.','I am unlike them.','Everyone likes images.','No one sees this image.','Liking is beyond beauty.','Liking is beyond quality.','What I like matters.','Everybody should be liked.','The right to be liked. ','The right to be alike.','You can’t be disliked.','You deserve to be liked','With a like you are not alone.','Forever not liked.','Forever liked.','No one owns this image.','I am the image.','The image is me.','Here you are, with no likes.','Is that you with no likes?','People like what I like.','I like this image.','First to like this image.','Like everybody.','Like is better than dislike.','I can’t dislike this image.','I would dislike this image.','What you like is not what you see.','What you like is not what you are.','I made this image for you.','Like it or not.','Image = information.','More is safer.','Less is suspect.','We need action.','We need quantity.','We need me.','Images are numbers.','Beauty scores.','Numbers equal power.','Exposure is liked.','Like one another.','You should like.','Like or not to like.','Like your bros.','Like your ass.','A like is worth 1000 words.','An image is worth 1000 likes.','I release people from their images.','Liking is the mission.','Liking is progress.','Solitude has been eradicated.','No likes equals no voice.','No one is left behind.','Agreeing is liberating.','To like is to be free.','Images united.','I turn images into likes.','I bring likes to people.','I improve images.','I approve images.','Likes are beautiful.','I only like the best.','To be liked more.','Best people make the best images.','To like is to be better.','To like is to improve.','You better be liked.','Liking is democracy.','I like that.','I like that too.','I like democracy.','The world needs more likes.','Never enough likes.','We can like more.','To be liked beyond existence.','I like you.','I am like you.','I like you too.','Like me in return.','Forever liked.','Feel like liking.','So like this.','I like it twice.','Deserves a like.','I forgot to like.','I liked the wrong image.','I take my like back.','I couldn’t like more.','Like a virgin.','The art of like.']

sentencesPlayed={}
imgDisplayed={}

def displayPic(url,levels):

	im = Image.open(url) #Can be many different formats.
	pix = im.load()
	colors=[]
	lines=[]
	for y in range(0,im.size[1]) :
		types=[]
		for x in range(0,im.size[0]) :
			moy=pix[x,y][0]+pix[x,y][1]+pix[x,y][2]
			moy=int(moy/3)
			moy=int(moy/int(256/len(levels)+1))
			types.append(moy)
		lines.append(types)

	for l in lines :
		line=''

		chunkY=0

		t=0
		while t < len(l) :
			line+=levels[l[t]]

			#line+=grayscale[int(moy/10)]
			t+=1
		time.sleep(0.05)
		print(line)
	'''
	ratio = 1

	l=0
	while l < len(lines) :
		chunks=''
		c=0
		while c < int(len(lines)/ratio) :

			chunk=0
			t=+c*ratio
			while t < ratio+c*ratio :
				chunk+=lines[l][int(t)]
				t+=1
			try :
				chunks+=grayscale[int(chunk/ratio)]
			except :
				ok=0

			c+=1
		time.sleep(0.025)
		print(chunks)
		l+=1
	'''
#displayPic('gradiant.png',grayscale)
#time.sleep(1)

while True :
	
	dirs=os.listdir('0lib/')
	choosenDir=dirs[int(random.random()*len(dirs))]
	pics=os.listdir('0lib/'+choosenDir)
	pic='0lib/'+choosenDir+'/'+pics[int(random.random()*len(pics))]

	findAllreadyCheckedPic=0

	while True :
		dirs=os.listdir('0lib/')
		choosenDir=dirs[int(random.random()*len(dirs))]
		pics=os.listdir('0lib/'+choosenDir)
		pic='0lib/'+choosenDir+'/'+pics[int(random.random()*len(pics))]
		try :
			imgDisplayed[pic]
			findAllreadyCheckedPic+=1
			if(findAllreadyCheckedPic>1000) :
				imgDisplayed={}
				break
		except :
			imgDisplayed[pic]=True
			break

	displayPic(pic,grayscale)
	
	findAllreadyCheckedSentences=0

	while True :
		choosenSentence = sentences[int(random.random()*len(sentences))]
		try :
			sentencesPlayed[choosenSentence]
			if(findAllreadyCheckedSentences>1000) :
				sentencesPlayed={}
				break
		except :
			sentencesPlayed[choosenSentence]=True
			break

	img = Image.open("sample_in.jpg")
	draw = ImageDraw.Draw(img)
	font = ImageFont.truetype("times.ttf", 24)
	draw.text((6, 0),choosenSentence,(255,255,255),font=font)
	img.save('sample-out.jpg')
	sentence='sample-out.jpg'
	print('')
	time.sleep(0.05)
	print('')
	time.sleep(0.05)
	print('')
	time.sleep(0.05)
	print('')
	time.sleep(0.05)
	displayPic(sentence,grayscale)
	displayPic('0likes.png',grayscale)
	print('')
	time.sleep(0.05)
	print('')
	time.sleep(0.05)
	print('')
	time.sleep(0.05)
	print('')
	time.sleep(0.05)
	time.sleep(1)

'''
displayPic("sentences/predator.png",radical)
displayPic("0lib/anglearis/18.jpg",grayscale)
displayPic("sentences/people.like.png",radical)
displayPic("0lib/anglearis/30.jpg",grayscale)
displayPic("sentences/are.these.png",radical)
displayPic("sentences/unseen.png",radical)
displayPic("sentences/mediocre.png",radical)
displayPic("0lib/nicola_sangalli_fitness_coach/20.jpg",grayscale)

l=0
while l < len(lines) :
	chunks=''
	c=0
	while c < int(len(lines)/ratio) :

		chunk=0
		t=+c*ratio
		while t < ratio+c*ratio :
			chunk+=lines[l][t]
			t+=1
		chunks+=grayscale[int(chunk/ratio)]
		c+=1
	print(chunks)
	l+=1







with open('data/data.json') as json_data:
    d = json.load(json_data)

users=[]

for i in d :
	for j in d[i] :
		for user in d[i][j] :
			users.append(user)

words = ['unseen','or','mediocrity','?','all','we','can','do','is','like','not','visible','or','mediocre?','rarity','of','the','unappreciated','positivism','engine','?','emptiness,','uncanny,','beyond','quality','pictures,','social','motivation','–','hunger','for','speed','or','quantity','–','exceeding','pictural','technique','or','art,','communication','tool,','calm','tech,','world','representation/','media','re-play,','TV,','magazine,','advertisement','?','picture','incest','the','nonsense','of','(social)','images?','too','much','sense?','beyond','quality?','infinite','scroll','empty','archive','in','consequence','of','the','decency','slow-bot','(non-sense)','archive','this','mysterious','100.000','instagram','users','list','(called','profile','directory)','non','alphabetical,','non','chronological,','non','geographical,','random,','not','very','old,','what','is','the','update','frequency','?','like','a','instagram','sample','(like','perfume','sample)','how','is','working','the','bot','making','this','archive','?','visual','essay','?','an','archive','from','an','archive,','a','bot','slowly','checking','for','non','liked','and','non','commented','pictures','and','not','finding','a','lot','potentially','unseen,','unappreciated,','real','indecency,','worst','content,','lost']
pictos = ['♥']
#grayscale=[' ','.',':','-','=','≡','░','#','▓','█']


while True :
	time.sleep(1)
	print('♥',words[int(random.random()*len(words))])
'''