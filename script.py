'''
#baptiste11ans

##0lib 

a bot scrapping pictures with no likes and no comments from instagram usernames
revealing the unseen or the not desired
'''
print('You just runned a bot scrapping pictures with no likes and no comments from instagram usernames. Revealing the unseen or the not desired')

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import sys
import json
import re
import requests
from pprint import pprint
from bs4 import BeautifulSoup
from collections import OrderedDict
from selenium.webdriver.common.action_chains import ActionChains
import time 
import os
import random
import datetime

# get instagram username database (d is for data)
print('get usernames database')
with open('data/data.json') as json_data:
    d = json.load(json_data)

# get wich user is all ready checked by the bot
print('get allready checked users database')
with open('data/usersChecked.json') as json_data:
    usersChecked = json.load(json_data)

# tell browser to run wihout display
print('tell browser to run wihout display')
options = Options()
options.add_argument("--headless")

# run firefox
print('run firefox')
driver = webdriver.Firefox(firefox_options=options)

#setting resolution
#print('setting resolution 1920*1080')
#driver.set_window_size(1920, 1080)

# put usernames from data to a list called users (because they are organised in 100 groups containing 100 sub-groups)
print('making usernames list')
users=[]
for i in d :
	for j in d[i] :
		for user in d[i][j] :
			users.append(user)

# run a loop ending when all users are checked
print('run a loop ending when all users are checked')
checked=0
while len(usersChecked) < len(users) :

	# choose an non checked user randomly
	print('choose an non checked user randomly')
	choice = 0
	while choice != 1 :
		pick = int(random.random()*len(users))
		user=users[pick]
		try :
			print(user,usersChecked[user])
		except :
			print(user,'not checked yet')
			choice = 1

	#start to check user instagram
	print('check',user,'instagram')
	driver.get('https://www.instagram.com/'+str(user)+'/')

	SCROLL_PAUSE_TIME = 0.25

	# Get scroll height
	last_height = driver.execute_script("return document.body.scrollHeight")

	scrolled=0
	postschecked=0
	pictures={}

	while True:
		# Scroll down to bottom
		print(datetime.datetime.now(),'scroll down in',user,'instagram',scrolled)
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

		# Wait to load page
		time.sleep(SCROLL_PAUSE_TIME)

		# Calculate new scroll height and compare with last scroll height
		new_height = driver.execute_script("return document.body.scrollHeight")
		scrolled+=1

		# get the pictures elements
		element_to_hover_over = driver.find_elements_by_class_name('_tn0ps');
		overId = 0
		overProcess=0

		# loop into the pictures elements
		if new_height != last_height or overProcess==0 :

			if overProcess==0 :
				overProcess=1

			for e in element_to_hover_over :

				picNotChecked=0

				try :
					print(pictures[element_to_hover_over[overId]])
				except :
					print('picture not checked yet')
					picNotChecked = 1

				if picNotChecked==1 :

					# mouse over a picture to display likes and comments
					try :
						driver.execute_script("arguments[0].scrollIntoView();", element_to_hover_over[overId])
						hover = ActionChains(driver).move_to_element(element_to_hover_over[overId]);
						hover.perform()
					except :
						print('can\'t hover')

					# get the like and comments display elements
					spans = element_to_hover_over[overId].find_elements_by_css_selector('span')
					isLikes=0
					isComments=0
					likes=''
					comments=''
					number=''

					# get the like and comments display elements
					for s in spans :
						# separate words from numbers (term likes / number of likes likes)
						try :
							int(float(s.text[0]))
							#print('@number',s.text)
							number = s.text.replace(" ", "")
							number = number.replace(",", "")
							number = number.replace("k", "00")
						except :
							#print('@word',s.text)
							if(s.text == 'J’aime') :
								isLikes=1
								likes=int(float(number))
							if(s.text == 'commentaires') :
								isComments=1
								comments=int(float(number))
					
					# check if this is a picture and not a video or something else
					if(isLikes==1 and isComments==1):
						print(str(checked)+'e user checked –','pseudo:',user,'–',str(postschecked)+'e post :',likes,'likes &',comments,'comments')

						# check if there is no likes and no comments
						if likes == 0 and comments == 0 :
							
							# make a directory for containing pictures from the user
							try :
								os.makedirs('0lib/'+str(user))
							except :
								print('')

							print('0likes picture found!',element_to_hover_over[overId].find_elements_by_css_selector('img')[0].get_attribute("src"))
							
							# get the picture url
							url = element_to_hover_over[overId].find_elements_by_css_selector('img')[0].get_attribute("src")
							r = requests.get(url, allow_redirects=True)

							# save the picture
							open('0lib/'+str(user)+'/'+str(overId)+'.jpg', 'wb').write(r.content)

					postschecked+=1
					pictures[element_to_hover_over[overId]]='checked'

				overId+=1

		#get the loading gif element to know if loading is over
		loading=driver.find_elements_by_class_name('_o5uzb')

		#stop scrolling if the user is fully loaded
		if new_height == last_height and loading==[] or scrolled>100:
			break
		last_height = new_height

	print('done scrolling')

	print(user,'checked')
	checked+=1
	usersChecked[user]='checked'

	#saved updated userschecked data
	with open('data/usersChecked.json', 'w') as outfile:
		json.dump(usersChecked, outfile)

# close the browser
driver.quit()
