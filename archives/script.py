from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import sys
import json
import re
import requests
from pprint import pprint
from bs4 import BeautifulSoup
from collections import OrderedDict
from selenium.webdriver.common.action_chains import ActionChains
import time 
import os
import random

with open('env/data.json') as json_data:
    d = json.load(json_data)

options = Options()
#options.add_argument("--headless")
driver = webdriver.Firefox(firefox_options=options)
print("Firefox Headless Browser Invoked")
driver.set_window_size(1920, 1080)

users=[]

for i in d :

	for j in d[i] :

		for user in d[i][j] :
			users.append(user)



usersChecked={}
checked=0

while len(usersChecked) < len(users) :

	choice = 0

	while choice != 1 :
		
		pick = int(random.random()*len(users))
		user=users[pick]

		try :
			print(user,usersChecked[user])
		except :
			print(user,'not checked yet')
			usersChecked[user]='checked'
			choice = 1

	print('scrapping',user)
	driver.get('https://www.instagram.com/'+str(user)+'/')

	SCROLL_PAUSE_TIME = 0.5

	# Get scroll height
	last_height = driver.execute_script("return document.body.scrollHeight")

	while True:
		# Scroll down to bottom
		print('scroll down in',user,'instagram')
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

		# Wait to load page
		time.sleep(SCROLL_PAUSE_TIME)

		# Calculate new scroll height and compare with last scroll height
		new_height = driver.execute_script("return document.body.scrollHeight")
		
		loading=driver.find_elements_by_class_name('_o5uzb')

		if new_height == last_height and loading==[]:
			break
		last_height = new_height

	print('done scrolling')

	# mouse hover pictures

	driver.set_window_size(1920, new_height)

	element_to_hover_over = driver.find_elements_by_class_name('_tn0ps');
	overId = 0

	for e in element_to_hover_over :
		hover = ActionChains(driver).move_to_element(element_to_hover_over[overId]);
		
		try :
			hover.perform()
		except :
			print('can\'t hover')

		spans = element_to_hover_over[overId].find_elements_by_css_selector('span')
		isLikes=0
		isComments=0
		likes=''
		comments=''
		number=''

		for s in spans :

			try :
				int(float(s.text[0]))
				#print('@number',s.text)
				number = s.text.replace(" ", "")
				number = number.replace(",", "")
				number = number.replace("k", "00")
			except :
				#print('@word',s.text)
				if(s.text == 'J’aime') :
					isLikes=1
					likes=int(float(number))
				if(s.text == 'commentaires') :
					isComments=1
					comments=int(float(number))
		
		if(isLikes==1 and isComments==1):
			print(str(checked)+'e user checked –','pseudo:',user,'–',str(overId)+'e post :',likes,'likes &',comments,'comments')

			if likes == 0 and comments == 0 :
					
				try :
					os.makedirs('0lib/'+str(user))
				except :
					print('')

				print('0likes picture found!',element_to_hover_over[overId].find_elements_by_css_selector('img')[0].get_attribute("src"))
				url = element_to_hover_over[overId].find_elements_by_css_selector('img')[0].get_attribute("src")
				r = requests.get(url, allow_redirects=True)
				open('0lib/'+str(user)+'/'+str(overId)+'.jpg', 'wb').write(r.content)

		overId+=1

	print(user,'checked')
	checked+=1

driver.quit()
