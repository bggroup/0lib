# Baptiste11ans

## Introduction

We started this work from two images: the first one "amateur" printed photograph probably from the 90s found in a book and the second one displayed by the searX search engine "by typing" the keywords *contemporary photograph*.

We wrote software that looks for and stores "unliked" and "uncommented" Instagram images.

These images are randomly selected and visually processed to be displayed on a terminal along with our comments, reflecting our views on the act of liking.

It is possible that these images have not been seen by anyone, a result of the effect of positivism embedded in this social network.

Baptiste11ans is the username of the Instagram profile that served as a reference to test our software.

## Some thoughts on liking

### Dislike as ignoring

Instagram only offers the possibility to like images (to agree only). The number of likes inevitably grows as an indication of the increasing value of these images.

'Likes' as a quantitative value that serve as an instrument to measure of interest and popularity.  'Likes' have a certain value and create value. It is the exchange currency within a social network.

Saying Yes without the possibility of saying No to an image raises questions about our response and involvement with social representations.

Instead of the possibility of expressing dislike we are only left with the possibility of scrolling i.e. ignoring. 

Disliking disolves and is replaced by an urge to continue to look for images.

Ignoring, instead of expressing dislike, gradually becomes the norm.

During a public Q&A session in December 2014, CEO Mark Zuckerberg (Facebook acquired Instagram in 2012) answered questions regarding the public's wish to have a dislike button on Facebook:

>There's something that's just so simple about the like button... but giving people more ways of expressing more emotions would be powerful. We need to figure out the right way to do it so it ends up being a force for good, not a force for bad and demeaning the posts that people are putting out there.

The use of the like button is simple, and opposition does not mix well with simplicity.  The company cannot afford the risk involved with offering the possibility of expressing disagreement.

Aversion is "a force for bad" it is a lot easier to agree. It is a positive feature for a website to provide ease of use, to encourage users to return regularly.

Having the possibility to dislike doesn't revolutionize social networks. Not having this choice banalizes the two options: click to like and ignore to dislike, as a result criminalizes our options of disliking.

The forced act of ignoring is perceived as neglecting and creates guilt.

In the end the ephemeral state of unliked content is more rare than the very liked one.
